var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
app.get('/api/prijava', function(req, res) {
	// ...
	res.send({status: "status", napaka: "Opis napake"});
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	console.log(req);
	res.send("<html><title>Naslov strani</title><body><p>Uporabnik <b>Krneki</b> nima pravice prijave v sistem!</p></body></html>");

});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];


var podatkiDatotekaStreznik = fs.readFileSync(__dirname +'/public/podatki/uporabniki_streznik.json', 'utf8');

function preveriSpomin(uporabniskoIme, geslo) {
	for(var i = 0; i < podatkiSpomin.length; i++){
		var moznost = podatkiSpomin[i].split('/');
		if(moznost[0] == uporabniskoIme && moznost[1] == geslo)
			return true;
		}
		return false;
}

function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	for(var i = 0; i < podatkiDatotekaStreznik.length; i++){
		var moznost = [podatkiDatotekaStreznik[i].uporabnik, podatkiDatotekaStreznik[i].geslo];
			if(moznost[0] == uporabniskoIme && moznost[1] == geslo)
				return true;
			}
			return false;
}